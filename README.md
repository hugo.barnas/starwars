# starwars


1. Object of this project
- Using the [api SWAP](https://swapi.dev/) for making an SPA (Single Page Application) in order to show characters, starchips and planets of the star wars saga.
- Using React js
- Using Redux 

2. Wireframes of this Application

    1. Overwiew  
![Capture_d_écran_2021-11-08_à_15.34.02](/uploads/819c3bfbd184e94233fbfe11f4abb14a/Capture_d_écran_2021-11-08_à_15.34.02.png)

    2. Register or sign-in page
![image](/uploads/32332eecce82d9994b9a0b9aa1958c89/image.png)

    3. Register
![image](/uploads/cc791dbf9d0a41ddc0799fd74fb1a67c/image.png)

    4. SignIn
![image](/uploads/4f96396c1262a5964f3a591e7ec4f1db/image.png)

    5. Users List
![image](/uploads/931f3a93596ed4882b3e3c8d93f826af/image.png)

    6. One user
![image](/uploads/7d1e2afdbe89664d5b93d5fd7cf5b2a8/image.png)

    7. Search page
![image](/uploads/3e390568700e1faf3e311862d1ce3a0c/image.png)

    9. People list
![image](/uploads/bd153e7e434313907ef0a0c4c2f94f4b/image.png)

    10. starships list
![image](/uploads/ad990f397679aa1f931b39ed0ce5e89d/image.png)

    11. Planets list
![image](/uploads/ed5927f892e5f1f576504b59253170e4/image.png)

    12. [Link to wireframes figma](https://www.figma.com/file/MAtq9PjjQvx3eQfjUQZBaN/Wireframe-Star-wars)

3. Technology used for the front 


![vite](/uploads/429b578a09805515ebe602148d1b96f6/vite.jpeg)vite : builder for make to in one file
![react](/uploads/a4d42dc60197591273f9e429d977e8de/react.png)React : Framework Js
![redux](/uploads/28446674c73f7afa714f694cec219f34/redux.png)Redux Toolkit : the state manager 
