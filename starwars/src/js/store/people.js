import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const apiSlice = createApi({
    reducerPath: 'api',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://swapi.dev/api/people'
    }),
    endpoints(builder) {
        return {
            fetchPeople: builder.query({
                query(limit = 10) {
                    return `?limit=${limit}`;
                }
            })
        }
    }
})

export const { useFetchPeopleQuery } = apiSlice