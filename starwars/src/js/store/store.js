import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './counterSlice'
import settingsReducer from './settings'
import { apiSlice } from './people'


export const store = configureStore({
    reducer: {
        counter: counterReducer,
        settings: settingsReducer,
        [apiSlice.reducerPath]: apiSlice.reducer
    },
})

console.log(store)