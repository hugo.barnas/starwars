import React, { useState } from 'react';
import { Card, Grid } from 'semantic-ui-react'

export function People({ data }) {
    const [searchTerm, setSearchTerm] = useState('')

    return (
        <>
            <h1>People</h1>
            <input type="text" placeholder='Search...' onChange={event => { setSearchTerm(event.target.value) }} />
            <Grid columns={3}>
                {data.filter(
                    (val) => {
                        if (searchTerm == "") {
                            return val
                        } else if (val.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                            return val
                        }
                    }).map((people, i) => {
                        return (
                            <Grid.Column key={i}>
                                <Card>
                                    <Card.Content>
                                        <Card.Header>{people.name}</Card.Header>
                                        <Card.Description>
                                            <strong>Heigth</strong>
                                            <p>{people.height}</p>
                                            <strong>Mass</strong>
                                            <p>{people.Mass}</p>
                                            <strong>Hair Color</strong>
                                            <p>{people.hair_color}</p>
                                        </Card.Description>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                        )
                    })}
            </Grid>
        </>
    );
}


export default People;
