import React from "react";
import { useSelector } from 'react-redux'

function Header() {
    const title = useSelector(state => state.settings.appName)
    return <p>{title}</p>

}
export default Header