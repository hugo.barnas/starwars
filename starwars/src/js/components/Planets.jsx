import React, { useState } from 'react';
import { Card, Grid } from 'semantic-ui-react'

export function Planets({ data }) {
    const [searchItem, setSearchItem] = useState('')
    return (
        <>
            <h1>Planets</h1>
            <input text='text' placeholder="Search..." onChange={event => { setSearchItem(event.target.value) }} />
            <Grid columns={3}>
                {data.filter(
                    (val) => {
                        if (searchItem == '') {
                            return val
                        }
                        else if (val.name.toLowerCase().includes(searchItem.toLowerCase())) {
                            return val
                        }
                    }
                ).map((planets, i) => {
                    return (
                        <Grid.Column key={i}>
                            <Card>
                                <Card.Content>
                                    <Card.Header>{planets.name}</Card.Header>
                                    <Card.Description>
                                        <strong>Climate</strong>
                                        <p>{planets.climate}</p>
                                        <strong>Diameter</strong>
                                        <p>{planets.diameter}</p>
                                        <strong>Population</strong>
                                        <p>{planets.population}</p>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                        </Grid.Column>
                    )
                })
                }
            </Grid>
        </>
    );
}

export default Planets;
