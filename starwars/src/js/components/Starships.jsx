import React, { useState } from 'react';
import { Card, Grid } from 'semantic-ui-react'

export function Starships({ data }) {
    const [searchTerm, setSearchTerm] = useState('')

    return (
        <>
            <h1>Starships</h1>
            <input type="text" placeholder='Search...' onChange={event => { setSearchTerm(event.target.value) }} />
            <Grid columns={3}>
                {data.filter(
                    (val) => {
                        if (searchTerm == "") {
                            return val
                        } else if (val.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                            return val
                        }
                    }).map((starships, i) => {
                        return (
                            <Grid.Column key={i}>
                                <Card>
                                    <Card.Content>
                                        <Card.Header>{starships.name}</Card.Header>
                                        <Card.Description>
                                        </Card.Description>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                        )
                    })}
            </Grid>
        </>
    );
}


export default Starships;
