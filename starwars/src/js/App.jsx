import React from 'react'
import { useState, useEffect } from 'react'
import Navbar from './components/Navbar'
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";

import { Container, Dimmer, Loader } from 'semantic-ui-react'


import '../css/App.css'
import Home from './components/Home'
import People from './components/People'
import Starships from './components/Starships'
import Planets from './components/Planets'


export function App() {
  const [people, setPeople] = useState([])
  const [planets, setPlanets] = useState([])
  const [starships, setStarships] = useState([])
  const [loading, setLoading] = useState([])


  useEffect(() => {
    async function fetchPeople() {
      let res = await fetch('https://swapi.dev/api/people/?format=json');
      let data = await res.json();

      //valeur d'arrivée
      setPeople(data.results)
      setLoading(false)
    }

    async function fetchPlanets() {
      let res = await fetch('https://swapi.dev/api/planets/?format=json');
      let data = await res.json();
      //valeur d'arrivée
      setPlanets(data.results)
      setLoading(false)
    }
    async function fetchStarships() {
      let res = await fetch('https://swapi.dev/api/starships/?format=json');
      let data = await res.json();
      //valeur d'arrivée
      setStarships(data.results)
      setLoading(false)
    }
    fetchPeople()
    fetchPlanets()
    fetchStarships()

  }, [])
  console.log('people :', people)
  console.log('planets :', planets)
  console.log('starships :', starships)


  return (
    <>
      <Router>
        <Navbar />
        <Container>
          {loading ? (
            <Dimmer active inverted>
              <Loader inverted>Loading</Loader>
            </Dimmer>
          ) : (
            <Routes>
              <Route exact path='/' element={<Home />} />
              <Route exact path='/People' element={<People data={people} />} />
              <Route exact path='/Planets' element={<Planets data={planets} />} />
              <Route exact path='/Starships' element={<Starships data={starships} />} />
            </Routes>
          )
          }
        </Container>
      </Router>
    </>
  )
}

export default App
